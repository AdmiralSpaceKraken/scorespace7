﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Controls
{
    public static KeyCode shoot;
    public static KeyCode slowDown;
    public static KeyCode pause;
}
