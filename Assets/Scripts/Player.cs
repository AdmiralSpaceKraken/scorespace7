﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : Entity
{
    [SerializeField] private float speed;
    [SerializeField] private float timeToReload;
    [SerializeField] private int damage;
    [SerializeField] private float timeTilNextAugmentation;
    [SerializeField] private int LeaderboardsIndex;

    private float augmentationTimer;
    private float reloadTimer;
    private bool isPaused = false;

    public Rigidbody2D rb;
    public GameObject bullet;
    public Text scoreText;
    public Text timerText;
    public Image healthBar;
    public GameObject pauseMenu;
    public Transform gunBarrel;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Score.currentScore = 0;
        Score.scoreMultiplier = 1.0f;

        //For testing purposes
        Controls.shoot = KeyCode.Mouse0; 
        Controls.slowDown = KeyCode.Space;
        Controls.pause = KeyCode.Escape;

        augmentationTimer = timeTilNextAugmentation;
    }

    private void Update()
    {
        //Pausing
        if(Input.GetKeyDown(Controls.pause))
            TogglePause();

        if(!isPaused)
        {
            Rotate();

            //Shooting
            if(Input.GetKey(Controls.shoot) && reloadTimer <= 0)
                Shoot();
            else
                reloadTimer -= Time.deltaTime;

            //Slow down player
            if(Input.GetKey(Controls.slowDown) || Input.GetMouseButton(1))
            {
                rb.drag = 8.0f;
            }
            else
                rb.drag = 0.0f;


            //Dealing with Augmentations
            augmentationTimer -= Time.deltaTime;
            if(augmentationTimer <= 0)
                NewAugmentation();

            //Updates the UI
            scoreText.text = "Score: " + Score.currentScore;
            timerText.text = "Time til next Level: " + Mathf.RoundToInt(augmentationTimer);
            healthBar.fillAmount = (hp / 100.0f);
        }
    }

    public void TogglePause()
    {
        if(isPaused)
        {
            isPaused = false;
            pauseMenu.SetActive(false);
            Time.timeScale = 1.0f;
        }
        else
        {
            isPaused = true;
            pauseMenu.SetActive(true);
            Time.timeScale = 0.0f;
        }
    }

    private void Rotate()
    {
        Vector2 positionOnScreen = Camera.main.WorldToViewportPoint(transform.position);
        Vector2 mouseOnScreen = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        float angle = Mathf.Atan2(positionOnScreen.y - mouseOnScreen.y, positionOnScreen.x - mouseOnScreen.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle + 90));
    }

    private void NewAugmentation()
    {
        //Pauses game
        //Shows augmentations on the screen and allows you to choose one
        //Reset augmentationTimer

        hp += 20;
        if(hp > 100)
            hp = 100;
        All.enemyFactory.NextRound();
        augmentationTimer = timeTilNextAugmentation;
    }

    private void Shoot()
    {
        //Finds the direction to shoot, then shoots a bullet in that direction
        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        if(direction.x != 0 || direction.y != 0) direction /= Mathf.Sqrt(direction.x * direction.x + direction.y * direction.y);

        GameObject rBullet = Instantiate(bullet, gunBarrel.position, Quaternion.identity);
        rBullet.GetComponent<Bullet>().OnSpawn(direction, "Enemy", damage);

        //Updates the players velocity accordingly
        rb.velocity += -direction * speed;

        reloadTimer = timeToReload;
    }

    //Upon being hit, the player takes damage
    public override void OnHit(int damageTaken)
    {
        hp -= damageTaken;
        if(hp < 0)
            OnDie();
    }

    private void OnDie()
    {
        //Saves score and loads the leaderboard
        SceneManager.LoadScene(LeaderboardsIndex);
    }
}
