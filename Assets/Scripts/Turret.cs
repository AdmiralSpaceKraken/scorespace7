﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : Enemy
{
    public Transform gunBarrel;

    private void Update()
    {
        if(reloadTimer <= 0)
            Shoot();
        else
            reloadTimer -= Time.deltaTime;

        Aim();
    }

    private void Aim()
    {
        Vector2 positionOnScreen = Camera.main.WorldToViewportPoint(transform.position);
        Vector2 playerOnScreen = Camera.main.WorldToViewportPoint(player.transform.position);
        float angle = Mathf.Atan2(positionOnScreen.y - playerOnScreen.y, positionOnScreen.x - playerOnScreen.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
    }

    private void Shoot()
    {
        Vector2 direction = player.transform.position - transform.position;
        if(direction.x != 0 || direction.y != 0) direction /= Mathf.Sqrt(direction.x * direction.x + direction.y * direction.y);

        GameObject rBullet = Instantiate(bullet, gunBarrel.position, Quaternion.identity);
        rBullet.GetComponent<Bullet>().OnSpawn(direction, "Player", damage, Color.red);
        rb.velocity += -direction * speed;

        reloadTimer = timeToReload;
    }
}
