﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Entity
{
    [SerializeField] protected float speed;
    [SerializeField] protected GameObject bullet;
    [SerializeField] protected int score;
    [SerializeField] protected float timeToReload;

    protected float reloadTimer;

    protected GameObject player; //Serialized only until EnemyFactory is made

    protected Rigidbody2D rb;
    [SerializeField] protected int damage;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public override void OnHit(int damageTaken)
    {
        hp -= damageTaken;
        if(hp < 0)
            OnDie();
    }

    private void OnDie()
    {
        Score.AddScore(score);
        Destroy(gameObject);
    }

    public void OnSpawn(GameObject _player)
    {
        player = _player;
        reloadTimer = timeToReload / 2;
        
    }
}
