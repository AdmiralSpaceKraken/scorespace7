﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Buttons : MonoBehaviour
{
    public void UnPause(GameObject player)
    {
        player.GetComponent<Player>().TogglePause();
    }

    public void LoadScene(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }

    public void Quit()
    {
        Score.SaveScores();
        Application.Quit();
    }

    public void SetMusicVolume()
    {
        Settings.musicVolume = gameObject.GetComponent<Slider>().value;
    }

}
