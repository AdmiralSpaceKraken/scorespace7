﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float timeAlive;

    private float deletionTime;
    private string targetTag;
    private int damage;

    public Rigidbody2D rb;
    public GameObject explosion;

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(other.tag);

        if(other.tag == targetTag)
        {  
            if(other.tag == "Enemy")
            {
                Instantiate(explosion, new Vector3(transform.position.x, transform.position.y, -2), Quaternion.identity);
            }

            other.GetComponent<Entity>().OnHit(damage);
            Destroy(gameObject);
        }

        if(other.tag == "Wall")
            Destroy(gameObject);
    }

    private void Update()
    {
        if(Time.time >= deletionTime)
            Destroy(gameObject);
    }

    public void OnSpawn(Vector2 direction, string _target, int _damage)
    {
        OnSpawn(direction, _target, _damage, Color.green);
    }


    public void OnSpawn(Vector2 direction, string _target, int _damage, Color newColor)
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = direction * speed;
        targetTag = _target;
        damage = _damage;
        deletionTime = Time.time + timeAlive;
        GetComponent<SpriteRenderer>().color = newColor;
    }
}
