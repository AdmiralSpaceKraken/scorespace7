﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFactory : MonoBehaviour
{
    [SerializeField] private float initialTimeToSpawn;
    [SerializeField] private Transform bottomLeft;
    [SerializeField] private Transform topRight;

    private List<Enemy> aliveEnemies = new List<Enemy>();
    private float spawnTimer;
    private int currentRound;
    [SerializeField] private float timeToSpawn; //Serialized for testing purposes

    public GameObject[] enemies;
    public GameObject player;
    public GameObject bullet;

    private void Awake()
    {
        All.enemyFactory = this;
        timeToSpawn = initialTimeToSpawn;
    }

    private void Start()
    {
        NextRound();
    }

    private void Update()
    {
        if(spawnTimer <= 0)
            SpawnNewEnemy();
        else
            spawnTimer -= Time.deltaTime;
    }

    public void NextRound()
    {
        Score.AddScore(currentRound * 1000);
        currentRound++;
        Score.scoreMultiplier += (currentRound / 10.0f);
        timeToSpawn = initialTimeToSpawn - (currentRound / 10.0f) + 0.1f;
    }

    private void SpawnNewEnemy()
    {
        GameObject newEnemy = Instantiate(GetRandomEnemy(), GetRandomPosition(), Quaternion.identity);
        newEnemy.GetComponent<Enemy>().OnSpawn(player);
        aliveEnemies.Add(newEnemy.GetComponent<Enemy>());
        spawnTimer = timeToSpawn;
    }

    private GameObject GetRandomEnemy()
    {
        int newEnemy = Random.Range(0, (enemies.Length > currentRound) ? currentRound : enemies.Length);
        return(enemies[newEnemy]);
    }

    private Vector2 GetRandomPosition()
    {
        return new Vector2(Random.Range(bottomLeft.position.x, topRight.position.x), Random.Range(bottomLeft.position.y, topRight.position.y));
    }

    public void OnEnemyDie(Enemy deadEnemy)
    {
        aliveEnemies.Remove(deadEnemy);
    }
}
