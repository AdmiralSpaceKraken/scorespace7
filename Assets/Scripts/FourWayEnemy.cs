﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FourWayEnemy : Enemy
{

    private Vector2[] directions = {Vector2.up, Vector2.down, Vector2.right, Vector2.left};

    private void Start()
    {
        rb.velocity = directions[Random.Range(0, 4)] * speed;
    }

    private void Update()
    {
        if(reloadTimer <= 0)
            Shoot();
        else
            reloadTimer -= Time.deltaTime;
    }

    private void Shoot()
    {
        foreach(Vector2 v2 in directions)
        {
            GameObject rBullet = Instantiate(bullet, transform.position, Quaternion.identity);
            rBullet.GetComponent<Bullet>().OnSpawn(v2, "Player", damage, Color.red);
        }

        reloadTimer = timeToReload;
    }
}
