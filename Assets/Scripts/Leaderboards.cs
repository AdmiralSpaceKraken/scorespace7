﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Leaderboards : MonoBehaviour
{
    [SerializeField] private Text currentScoreText;
    [SerializeField] private Text highScoreText;

    private void Start()
    {
        Score.LoadScores();
        Score.CheckScore();

        currentScoreText.text = "Your Score: " + Score.currentScore;
        highScoreText.text = "High Scores: \n";
        for(int i = 0; i < 5; i++)
        {
            int x = i + 1;
            highScoreText.text += x + ": " + Score.highScores[i] + "\n";
        }
        
        Score.SaveScores();
    }
}
