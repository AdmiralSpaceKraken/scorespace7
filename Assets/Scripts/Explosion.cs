﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{    
    [SerializeField] private float timeAlive;

    private float deletionTime;

    void Start()
    {
        deletionTime = Time.time + timeAlive;
    }

    void Update()
    {
        if(Time.time >= deletionTime)
            Destroy(gameObject);
    }
}
