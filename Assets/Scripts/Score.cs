﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Score
{
    public static int currentScore;

    public static float scoreMultiplier;

    public static int[] highScores = new int[5];

    public static void CheckScore()
    {
        if(currentScore > highScores[0])
        {
            highScores[4] = highScores[3];
            highScores[3] = highScores[2];
            highScores[2] = highScores[1];
            highScores[1] = highScores[0];
            highScores[0] = currentScore;
        }
        else if(currentScore > highScores[1])
        {
            highScores[4] = highScores[3];
            highScores[3] = highScores[2];
            highScores[2] = highScores[1];
            highScores[1] = currentScore;
        }
        else if(currentScore > highScores[2])
        {
            highScores[4] = highScores[3];
            highScores[3] = highScores[2];
            highScores[2] = currentScore;
        }
        else if(currentScore > highScores[3])
        {
            highScores[4] = highScores[3];
            highScores[3] = currentScore;
        }
        else if(currentScore > highScores[4])
            highScores[4] = currentScore;
    }

    public static void SaveScores()
    {
        for(int i = 4; i >= 0; i--)
        {
            PlayerPrefs.SetInt("Score" + i, highScores[i]);
        }
    }

    public static void LoadScores()
    {
        for(int i = 4; i >= 0; i--)
        {
            highScores[i] = PlayerPrefs.GetInt("Score" + i, 0);
        }
    }

    public static void AddScore(int points)
    {
        currentScore += Mathf.RoundToInt(points * scoreMultiplier);
    }
}
